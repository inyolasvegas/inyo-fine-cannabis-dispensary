Inyo Fine Cannabis Dispensary opened to the public on October 1, 2015. It was founded by two local Las Vegans who wanted to provide their neighbors the opportunity to safely access premium quality marijuana at affordable prices.

Address : 2520 S Maryland Pkwy, #2, Las Vegas, NV 89109

Phone : 702-707-8888